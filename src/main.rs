use macroquad::prelude::*;

#[derive(Copy, Clone, Debug, PartialEq)]
enum PixelKind
{
    EMPTY,
    SAND,
    WATER,
    STONE,
}

const WIN_W: f32 = 1920.0;
const WIN_H: f32 = 1080.0;
const PIXEL_SIZE_WIDTH: usize = (WIN_W / 5.0) as usize;
const PIXEL_SIZE_HEIGHT: usize = (WIN_H / 5.0) as usize;

struct Board
{
    pixels: [PixelKind; PIXEL_SIZE_HEIGHT*PIXEL_SIZE_WIDTH],
}

impl Board
{
    fn new() -> Self
    {
        let pixels = [PixelKind::EMPTY; PIXEL_SIZE_HEIGHT* PIXEL_SIZE_WIDTH];
        return Board
        {
            pixels,
        };
    }
    fn render(&self)
    {
        for i in 0..self.pixels.len()
        {
            draw_rectangle((i % PIXEL_SIZE_WIDTH * 5) as f32, (i / PIXEL_SIZE_WIDTH * 5) as f32, 5.0, 5.0, get_color(self.pixels[i]));
        }
    }
    fn position(x: f32, y: f32) -> usize
    {
        return (x / 5.0 + (f32::floor(y / 5.0) * PIXEL_SIZE_WIDTH as f32)) as usize;
    }
    fn update(&mut self)
    {
        let mut iter = 0;
        for i in self.pixels
        {
            match i
            {
                PixelKind::SAND => 
                {
                    let mut below = iter + PIXEL_SIZE_WIDTH;
                    if below <= PIXEL_SIZE_WIDTH * PIXEL_SIZE_HEIGHT - 2
                    {
                        if self.pixels[below] == PixelKind::EMPTY
                        {
                            self.pixels[iter] = PixelKind::EMPTY;
                            self.pixels[below] = PixelKind::SAND;
                        }
                        else if self.pixels[below - 1] == PixelKind::EMPTY
                        {
                            self.pixels[iter] = PixelKind::EMPTY;
                            self.pixels[below - 1] = PixelKind::SAND;
                        }
                        else if self.pixels[below + 1] == PixelKind::EMPTY
                        {
                            self.pixels[iter] = PixelKind::EMPTY;
                            self.pixels[below + 1] = PixelKind::SAND;
                        }
                    }
                },
                _ => {},
            }
            iter += 1;
        }
    }
}

fn get_color(kind: PixelKind) -> macroquad::color::Color
{
    match kind
    {
        PixelKind::EMPTY =>
        {
            return BLACK;
        }
        PixelKind::SAND =>
        {
            return YELLOW;
        }
        PixelKind::WATER =>
        {
            return BLUE;
        }
        PixelKind::STONE =>
        {
            return GRAY;
        }
    }
}

#[macroquad::main("pixel_sim")]
async fn main()
{
    let mut board = Board::new();
    loop
    {
        let (mouse_x, mouse_y) = mouse_position();

        if is_mouse_button_down(MouseButton::Left)
        {
            let board_pos = Board::position(mouse_x, mouse_y);
            board.pixels[board_pos] = PixelKind::SAND;
        }

        if is_mouse_button_down(MouseButton::Right)
        {
            let board_pos = Board::position(mouse_x, mouse_y);
            board.pixels[board_pos] = PixelKind::STONE;
        }

        board.update();
        clear_background(BLUE);
        board.render();
        next_frame().await
    }
}

